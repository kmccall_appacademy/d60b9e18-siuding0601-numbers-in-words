class Fixnum

  def zero_to_nine(num)
    array = %w(zero one two three four five six seven eight nine)
    array[num]
  end

  def ten_nineteen(num)
    array = %w(ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen)
    array[num - 10]
  end

  def the_tys(num)
    array = %w(twenty thirty forty fifty sixty seventy eighty ninety)
    array[num/10 -2]
  end

  def hundreds(num)
    zero_to_nine()
  end

  def combine(num)
    org = num.to_s.split("")
    each_num = org.clone
    length = each_num.count
    ans = []
    count = 0
      while length > 2
        store = each_num.pop(3)
        temp_ans = []
        count += 3
        two_d_num = (store[1]+store[2]).to_i

          if store[0] != "0"
            temp_ans.unshift((zero_to_nine(store[0].to_i)))
            temp_ans<< ("hundred")
          end

          if two_d_num > 10 && two_d_num < 20
          temp_ans << (ten_nineteen(two_d_num))
          elsif store[1]  != "0"
            temp_ans << (the_tys(store[1].to_i*10))
            temp_ans << (zero_to_nine(store[2].to_i)) unless store[2] == "0"
          elsif store[2].to_i > 0
            temp_ans << (zero_to_nine(store[2].to_i))
          end

          ans.unshift(temp_ans)

          if each_num == []
            return ans.join(" ")
          end

            if !ans.include?("thousand")
              ans.unshift("thousand")
            elsif !ans.include?("million")
              ans.unshift("million")
            elsif !ans.include?("billion")
              ans.unshift("billion")
            else
              ans.unshift("trillion")
            end
          length -= 3
      end

        if each_num.join.to_i >= 10 && each_num.join.to_i < 20
          ans.unshift(ten_nineteen(each_num[1].to_i))
        elsif each_num[0]  != "0" && each_num.count > 1
          ans.unshift(zero_to_nine(each_num[-1].to_i))
          ans.unshift(the_tys(each_num[0].to_i*10))
        else
          ans.unshift(zero_to_nine(each_num[-1].to_i))
        end

        if org.count > 6 && org[-6..-4].join.to_i == 0
          ans.delete("thousand")
        end

        if org.count > 9 && org[-9..-7].join.to_i == 0
          ans.delete("million")
        end

        if org.count > 12 && org[-12..-10].join.to_i == 0
          ans.delete("billion")
        end

        ans.delete([])
        ans.join(" ")
  end




  def in_words
    if self < 10
      zero_to_nine(self)
    elsif self < 20
      ten_nineteen(self)
    elsif self%10 == 0 && self < 100
      the_tys(self)
    elsif self < 100
      the_tys(self) + " " + zero_to_nine(self % 10)
    else
      combine(self)
    end
  end

end
